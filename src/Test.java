public class Test {
    static double pow(double base, int p) {
        if (p < 0) {
            return 1 / pow(base, -p);
        }

        if (0 == p) {
            return 1;
        }

        return base * pow(base, p-1);
    }

    static double fact(double n) {
        return (n <= 0) ? 1 : n * fact(n-1);
    }

    static int fib(int n) {
        if (n < 2) {
            return 1;
        }

        return fib(n-1) + fib(n-2);
    }

    public static void main(String[] args) {
        System.out.println("pow:");
        System.out.println( pow(2, -2) );
        System.out.println( pow(2, -1) );
        System.out.println( pow(2, 0) );
        System.out.println( pow(2, 1) );
        System.out.println( pow(2, 2) );
        System.out.println( pow(2, 3) );

        System.out.println("fact:");
        System.out.println( fact(0) );
        System.out.println( fact(1) );
        System.out.println( fact(2) );
        System.out.println( fact(3) );
        System.out.println( fact(4) );
        System.out.println( fact(5) );

        System.out.println("fib:");
        System.out.println( fib(0) );
        System.out.println( fib(1) );
        System.out.println( fib(2) );
        System.out.println( fib(3) );
        System.out.println( fib(4) );
        System.out.println( fib(5) );
    }
}
